% Author: Kilián Imre
% Date: 2011.09.01.
%
% test warshall's algorithm with PCLOG
% working directory should point to PCLOG home

:- use_module(pack(pclog/prolog/clog)).

:- use_module(pack(pclog/tests/warshall)).


warshall:w2Display(MAX):-
  between(0,MAX,K), nl,
  between(1,MAX,I),
  findall(W,(between(1,MAX,J),
	     warshall:wm(K,I,J,W,MAX)), XL), writeln(XL), fail;
  true.



:- write('To start, type\n:- warshall:start.'),nl.


warshall:start:-
  warshall:clean, fail;
  warshall:fire_w([[100,100,-2,100],
		   [4,100,3,100],
		   [100,100,100,2],
		   [100,-1,100,100]
		   ]), fail;
  warshall:w2Display(4).













