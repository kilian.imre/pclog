:- module(p3,[]).

:- contra.

:- export([pyth3/3]).

%searching for triads of pythagoras
pyth3(X,Y,Z):-
  natural3(X,Y,Z,_), {SUM2 is X*X+Y*Y, SUM2 is Z*Z}.

%searching for triads such that X<Y, X+Y<SUMXY, Y<Z<SUMXY
natural3(X,Y,Z,SUMXZ):-
  natural2(Y,Z,SUMXZ), {X is Y-1, X>0}.
natural3(X,Y,Z,SUMXZ):-
  natural3(X1,Y,Z,SUMXZ), {X is X1-1, X>0}.

%producing natural number pairs such that 0<X<Y, and X+Y=SUM
natural2(0,SUM,SUM):- natural(SUM).
natural2(X,Y,SUM):-
  natural2(X1,Y1,SUM),
  {X1<SUM, X is X1+1, Y is Y1-1, X<Y}.

%producing natural numbers
natural(1).
natural(X):- natural(X1), {X is X1+1}.

:- pro.


