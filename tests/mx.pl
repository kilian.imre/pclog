:- module(mx,[]).

arg0(X0,TERM,ARG):- X is X0+1, arg(X,TERM,ARG).

mxyz(LEFT,MID,RIGHT,MULT):- vector(TERM),
	arg0(LEFT,TERM,LX),
	arg0(MID,TERM,MX), RIGHT1 is RIGHT+1,
	arg0(RIGHT1,TERM,RX), MULT is LX*MX*RX.

:- contra.

:- key(matrix(+,+,-,-)).

:-import([vector/1]).


matrix(SIZE,SIZE,0,0):- vector(TERM),
	{functor(TERM,_,SIZ), SIZE is SIZ-2}.
matrix(I,I,X,C):- matrix(I0,I0,X,C), {I is I0-1, I>=0}.

matrix(I,J,X,J1):-
	matrix(I,J1,Y,_), matrix(I1,J,Z,_), {I1 =:= J1+1},
	{mxyz(I,I1,J,MULT), X is Y+Z+MULT,
	  (matrix(I,J,X0,_)->X<X0;true)}.


:- pro.












