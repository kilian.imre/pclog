% Author: Kilián Imre
% Date: 2011.09.01.
%
% test matrix chain product with PCLOG
% working directory should point to PCLOG home
%consult pclog
:- use_module(pack(pclog/prolog/clog)).

:- use_module(pack(pclog/tests/mx)).

:- export(m2Display/1).

mx:m2Display(MAX):-
  between(0,MAX,II), I is MAX-II,
  findall(X,(between(0,I,J), mx:matrix(J,I,X,_)), XL), writeln(XL), fail;
  between(0,MAX,II), I is MAX-II,
  findall(X,(between(0,I,J), mx:matrix(J,I,_,X)), XL), writeln(XL), fail; true.



:- write('To start, type:\n:-mx:start.'),nl.


mx:start:-
  mx:clean, fail;
  mx:fire_vector(mx(30,35,15,5,10,20,25)), fail;
  mx:m2Display(5).













