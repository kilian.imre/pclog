:- module(ctrace,[trstart/1]).

:- use_module(pack(pclog/prolog/clog)).

:- dynamic option/2.

trstart(GOAL):- trfire(0,GOAL).

trfire(I,M:FG):- trfire(I,M,FG).

trfire(I,_,M:FG):- !, trfire(I,M,FG).
trfire(I,M,FG):-
	clog:structure_to_fire(G,FG),
	(trdisplay('>',I,G),
	clause(M:FG,ASSERTEST),
	(ASSERTEST=(assert(ASSERT),TEST)->
	      trdisplay('#',I,ASSERT),
	      assert(M:ASSERT), I1 is I+1,
	      trtest(I1,M,TEST);
	fail);
	trdisplay('+',I,G)).

trtest(I,_,M:TEST):- !, trtest(I,M,TEST).
trtest(I,M,TEST):-
	clause(M:TEST,BODY),
	(clog:structure_to_export(EXP,BODY)->
	    trdisplay('@',I,EXP), M:EXP;
	trbody(I,M,BODY)).

trbody(I,M0,(M:CALL,BODY)):- !, M:CALL, trbody(I,M0,BODY).
trbody(I,M,(CALL,BODY)):- !,
	trdisplay('<',I,CALL),
	(M:CALL,trdisplay('+',I,CALL);
	trdisplay('-',I,CALL),fail),
	trbody(I,M,BODY).
trbody(I,M,FIRE):- trfire(I,M,FIRE).

trdisplay(MARK,INDENT,GOAL):-
  option(port,PORTS), \+ memberchk(MARK,PORTS), !;
  tab(INDENT), write(MARK), write(GOAL), nl.


setOption(port,VALUE):-
   retract(option(port,_)), fail;
   assert(option(port,VALUE)).


%:- setOption(port,[+,-,>,<,#,?,@]).

:- setOption(port,[+,>,<,@]).










